ARG BASE
FROM ${BASE}

ENV DEBIAN_FRONTEND=noninteractive

ARG PACKAGES
# arch where we are building for (i.e. the cross arch)
ARG HOSTARCH

# Skip package installation if arch to build for is the same as the native. For
# multi-arch images, only the foreign one will need a cross toolchain.
RUN if [ "$(uname -m)" != "${HOSTARCH}" ]; then apt-get update \
    && apt-get install \
        --assume-yes \
        --no-install-recommends \
        --option=debug::pkgProblemResolver=yes \
        $PACKAGES; \
    fi

# For gcc images, symlink all binaries with unversioned names into $PATH
RUN for bin in $(ls -1 /usr/bin/*-[0-9]* | xargs -n 1 basename); do \
    unver=${bin%-[0-9]*}; \
    if [ ! -x /usr/bin/${unver} ]; then \
        ln -sfv /usr/bin/$bin /usr/local/bin/${unver}; \
        fi; \
    done

# For clang images, symlink all the binaries in $PATH
RUN if [ -d /usr/lib/llvm-*/bin ]; then ln -sfv /usr/lib/llvm-*/bin/* /usr/local/bin/; fi

# The GCC plugins have no default package name, so we must discover the installed
# GCC version in order to install them. :(
RUN apt-get install \
        --assume-yes \
        --no-install-recommends \
        --option=debug::pkgProblemResolver=yes \
        $(dpkg -l 'gcc-*' | grep ^.i | awk '{print $2}' | \
          grep -E '^gcc-[0-9]*-(.*)-base(:.*)*$' | grep -v -- -cross-base | \
          sed -e 's/^gcc-\([0-9]*\)-/gcc-\1-plugin-dev-/; s/-base//;')

# vim: ft=dockerfile
